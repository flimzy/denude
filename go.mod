module gitlab.com/flimzy/denude

go 1.22.0

require (
	github.com/rogpeppe/go-internal v1.12.0
	golang.org/x/tools v0.18.0
	honnef.co/go/tools v0.4.7
)

require (
	golang.org/x/exp/typeparams v0.0.0-20221208152030-732eee02a75a // indirect
	golang.org/x/mod v0.15.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
