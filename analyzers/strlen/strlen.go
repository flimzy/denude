package strlen

import (
	"fmt"
	"go/ast"
	"go/token"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "strlen",
		Doc:  "Replaces string length checks with empty string checks",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

func (a *analyzer) inspect(node ast.Node) bool {
	expr, ok := node.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	x, op, y := expr.X, expr.Op, expr.Y
	if internal.IsLiteralZero(x) {
		// Swap x and y so that we can handle cases like 0 < len(x)
		x, y = y, x
		switch op {
		case token.LSS:
			op = token.GTR
		case token.LEQ:
			op = token.GEQ
		case token.GTR:
			op = token.LSS
		case token.GEQ:
			op = token.LEQ
		case token.NEQ, token.EQL:
		default:
			return false
		}
	}
	if !internal.IsLiteralZero(y) {
		return true
	}
	arg, ok := internal.LenOf(a.pass, x)
	if !ok {
		return true
	}
	if !internal.IsString(a.pass, arg) {
		return true
	}

	var newOp token.Token
	switch op {
	case token.NEQ, token.GTR, token.GEQ:
		newOp = token.NEQ
	case token.EQL:
		newOp = token.EQL
	default:
		return true
	}

	replacement := &ast.BinaryExpr{
		X:  arg,
		Op: newOp,
		Y:  &ast.BasicLit{Kind: token.STRING, Value: `""`},
	}

	oldText := internal.DumpNode(expr)
	newText := internal.DumpNode(replacement)

	a.pass.Report(analysis.Diagnostic{
		Pos:     expr.Pos(),
		End:     expr.End(),
		Message: fmt.Sprintf("string literal can be simplified as raw string: %s => %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     expr.Pos(),
						End:     expr.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following Sprint calls that can be simplified:
//
// TODO:
//
//	if len(s) != 0 { ==> if s != "" {
//	if len(s) == 0 { ==> if s == "" {
//	if len(s) > 0 { ==> if s != "" {
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
