package lenloopguard

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/token"
	"go/types"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the lenloopguard analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "lenloopguard",
		Doc:  "removes unnecessary len guards around slice/map loops",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

func isNil(expr ast.Expr) bool {
	id, ok := expr.(*ast.Ident)
	if !ok {
		return false
	}
	return id.Name == "nil"
}

// isSliceOrMap returns true if expr is rerpresents a slice or map variable.
func (a *analyzer) isSliceOrMap(expr ast.Expr) (string, bool) {
	typ := a.pass.TypesInfo.TypeOf(expr)
	if typ == nil {
		return "", false
	}
	switch typ.Underlying().(type) {
	case *types.Slice:
		return "slice", true
	case *types.Map:
		return "map", true
	}
	return "", false
}

// isLoopGuard returns true if cond represents a "loop guard clause", and the
// slice/map it guards. The following conditions are considered loop guards,
// where x is a slice or map:
//
//	x != nil
//	len(x) > 0
//	len(x) >= 0
//	len(x) != 0
//	nil != x
//	0 < len(x)
//	0 <= len(x)
//	0 != len(x)
func (a *analyzer) isLoopGuard(cond *ast.BinaryExpr) (string, ast.Expr, bool) {
	x, op, y := cond.X, cond.Op, cond.Y

	// Quick check whether we might possibly care about the operator
	switch op {
	case token.LSS, token.LEQ:
		if !internal.IsLiteralZero(x) {
			return "", nil, false
		}
		// Swap operands to normalize the expression form
		x, y = y, x
	case token.NEQ:
		if internal.IsLiteralZero(x) || isNil(x) {
			// Swap operands to normalize the expression form
			x, y = y, x
		}
	case token.GTR, token.GEQ:
		// Allow these as-is
	default:
		return "", nil, false
	}
	if varTyp, ok := a.isSliceOrMap(x); ok {
		return varTyp, x, isNil(y) && op == token.NEQ
	}
	arg, ok := internal.LenOf(a.pass, x)
	if !ok {
		return "", nil, false
	}
	ident, ok := arg.(*ast.Ident)
	if !ok {
		return "", nil, false
	}
	varTyp, ok := a.isSliceOrMap(ident)
	if !ok {
		return "", nil, false
	}
	return varTyp, arg, internal.IsLiteralZero(y)
}

// isLenCheck returns true if expr is a len check in one of the following forms,
// where i is the loop iterator, and sm is a slice or map:
//
//	i < len(sm)
//	i <= len(sm)
func (a *analyzer) isLenCheck(i, sm, expr ast.Expr) bool {
	cond, ok := expr.(*ast.BinaryExpr)
	if !ok {
		return false
	}
	x, op, y := cond.X, cond.Op, cond.Y
	if (op == token.LSS || op == token.LEQ) && internal.NamesMatch(i, x) {
		arg, ok := internal.LenOf(a.pass, y)
		if !ok {
			return false
		}
		return internal.NamesMatch(sm, arg)
	}
	if (op == token.GTR || op == token.GEQ) && internal.NamesMatch(i, y) {
		arg, ok := internal.LenOf(a.pass, x)
		if !ok {
			return false
		}
		return internal.NamesMatch(sm, arg)
	}
	return false
}

// isLoopOver returns true if body represents a single for/range loop that
// loops over the slice/map sm.
//
// It will return true for the following cases:
//
//	for i := range sm { ... }
//	for range sm { ... }
//	for x, y := range sm { ... }
//
// TODO:
//
//	for i := 0; i < len(sm); i++ { ... }
//	for i := 0; i < len(sm); i += 1 { ... }
//	for i := 0; i < len(sm); i = i + 1 { ... }
func (a *analyzer) isLoopOver(sm ast.Expr, body *ast.BlockStmt) bool {
	if len(body.List) != 1 {
		return false
	}
	switch loopStmt := body.List[0].(type) {
	case *ast.RangeStmt:
		return internal.NamesMatch(sm, loopStmt.X)
	case *ast.ForStmt:
		assignment, ok := loopStmt.Init.(*ast.AssignStmt)
		if !ok {
			return false
		}
		if len(assignment.Lhs) != 1 {
			return false
		}
		if !internal.IsLiteralZero(assignment.Rhs[0]) {
			return false
		}
		if !a.isLenCheck(assignment.Lhs[0], sm, loopStmt.Cond) {
			return false
		}
		if loopStmt.Post == nil {
			return false
		}
		return internal.IsEffectiveIncByOne(assignment.Lhs[0], loopStmt.Post)
	}
	return false
}

func (a *analyzer) inspect(node ast.Node) bool {
	ifStmt, ok := node.(*ast.IfStmt)
	if !ok {
		return true
	}
	if ifStmt.Init != nil {
		return true
	}
	if ifStmt.Else != nil {
		return true
	}
	cond, ok := ifStmt.Cond.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	varTyp, sm, ok := a.isLoopGuard(cond)
	if !ok {
		return true
	}

	if !a.isLoopOver(sm, ifStmt.Body) {
		return true
	}

	oldText := internal.DumpNode(ifStmt)

	a.pass.Report(analysis.Diagnostic{
		Pos: ifStmt.Pos(),
		End: ifStmt.End(),
		Message: fmt.Sprintf("unnecessary guard around loop over %s: %s",
			varTyp,
			string(bytes.SplitN(oldText, []byte("\n"), 2)[0]),
		),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     ifStmt.Pos(),
						End:     ifStmt.Body.List[0].Pos(),
						NewText: nil,
					},
					{
						Pos:     ifStmt.Body.List[0].End(),
						End:     ifStmt.End(),
						NewText: nil,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following redundant nil-checks:
//
//	if len(s) > 0 { for i := 0; i < len(s); i += 1 { ... } }
//	if 0 =< len(s) { for i := 0; i < len(s); i += 1 { ... } }
//
// TODO:
//
//	if s := slice(); len(s) > 0 { for i := 0; i < len(s); i += 1 { ... } }
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
