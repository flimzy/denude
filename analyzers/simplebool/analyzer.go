package simplebool

import (
	"bytes"
	"go/ast"
	"go/printer"
	"go/token"
	"go/types"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

/*
TODO:
- Remove unnessary parenthesis: eg. case (1+1 > 3)
- || and && with non-bool-literals
*/

const (
	litTrue  = "true"
	litFalse = "false"
)

// Analyzer is the denude analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "simplebool",
		Doc:  "simplifies boolean expressions with bool literals",
		Run:  run,
	}
}

func run(pass *analysis.Pass) (any, error) {
	inspect := func(node ast.Node) bool {
		switch n := node.(type) {
		case *ast.BinaryExpr:
			simplifyBinaryExprWithBoolLiteral(pass, n)
		case *ast.UnaryExpr:
			simpler, ok := simplifyNot(n)
			if !ok {
				break
			}
			var buf bytes.Buffer
			if err := printer.Fprint(&buf, pass.Fset, simpler); err != nil {
				panic(err)
			}
			pass.Report(analysis.Diagnostic{
				Pos:     n.Pos(),
				End:     n.End(),
				Message: "should simplify unary expression",
				SuggestedFixes: []analysis.SuggestedFix{
					{
						TextEdits: []analysis.TextEdit{
							{
								Pos:     n.Pos(),
								End:     n.End(),
								NewText: buf.Bytes(),
							},
						},
					},
				},
			})
		}

		return true
	}

	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, inspect)
	}
	return nil, nil
}

func boolExpr(n ast.Node) (val, ok bool) {
	if ident, ok := n.(*ast.Ident); ok && ident.Obj == nil {
		switch ident.Name {
		case litTrue:
			return true, true
		case litFalse:
			return false, true
		}
	}
	return false, false
}

func newBoolExpr(val bool) ast.Expr {
	if val {
		return ast.NewIdent(litTrue)
	}
	return ast.NewIdent(litFalse)
}

// simplifyNot looks for double NOTs (i.e. !!x) or negated bool literals
// (i.e. !true) and simplifies them. If the expression does not match one of
// these heuristics, it returns the original expression and false.
func simplifyNot(val ast.Expr) (ast.Expr, bool) {
	unary, ok := val.(*ast.UnaryExpr)
	if !ok || unary.Op != token.NOT {
		return val, false
	}
	switch inner := unary.X.(type) {
	case *ast.UnaryExpr:
		if inner.Op == token.NOT {
			return inner.X, true
		}
	case *ast.Ident:
		if inner.Obj == nil {
			switch inner.Name {
			case litTrue:
				return ast.NewIdent(litFalse), true
			case litFalse:
				return ast.NewIdent(litTrue), true
			}
		}
	}
	return val, false
}

func simplifyBinaryExprWithBoolLiteral(pass *analysis.Pass, n *ast.BinaryExpr) {
	lhs, lok := boolExpr(n.X)
	rhs, rok := boolExpr(n.Y)
	var replacement ast.Expr // Placeholder for replacement expression
	switch {
	case !lok && !rok:
		// No bool literals on either side, so nothing to do
		return
	case lok && rok:
		// Both sides are bool literals, so let's handle the easy cases first
		switch n.Op {
		case token.LAND:
			replacement = newBoolExpr(lhs && rhs)
		case token.LOR:
			replacement = newBoolExpr(lhs || rhs)
		case token.EQL:
			replacement = newBoolExpr(lhs == rhs)
		case token.NEQ:
			replacement = newBoolExpr(lhs != rhs)
		}
	default:
		var constBool bool
		if !lok {
			constBool = rhs
			replacement = n.X
		} else {
			constBool = lhs
			replacement = n.Y
		}

		if isInterfaceType(pass, replacement) {
			return
		}

		var positive bool
		switch n.Op {
		case token.EQL:
			positive = constBool
		case token.NEQ:
			positive = !constBool
		default:
			return
		}
		if !positive {
			replacement = &ast.UnaryExpr{
				OpPos: n.Pos(),
				Op:    token.NOT,
				X:     replacement,
			}
		}
	}

	replacement, _ = simplifyNot(replacement)
	var buf bytes.Buffer
	if err := printer.Fprint(&buf, pass.Fset, replacement); err != nil {
		panic(err)
	}
	pass.Report(analysis.Diagnostic{
		Pos:     n.Pos(),
		End:     n.Y.End(),
		Message: "should rewrite comparison to bool literal",
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     n.Pos(),
						End:     n.Y.End(),
						NewText: buf.Bytes(),
					},
				},
			},
		},
	})
}

// isInterfaceType checks whether the replacement represents an interface value,
// in which case we can't simplify the expression.
func isInterfaceType(pass *analysis.Pass, replacement ast.Node) bool {
	switch repIdent := replacement.(type) {
	case *ast.Ident:
		obj := pass.TypesInfo.Uses[repIdent]
		if obj != nil {
			if _, ok := obj.Type().(*types.Interface); ok {
				return true
			}
		}
	case *ast.IndexExpr:
		obj := pass.TypesInfo.Types[repIdent]
		if obj.Type != nil {
			if _, ok := obj.Type.(*types.Interface); ok {
				return true
			}
		}
	case *ast.CallExpr:
		typeAndValue := pass.TypesInfo.Types[repIdent]
		if typeAndValue.Type != nil {
			if _, ok := typeAndValue.Type.(*types.Interface); ok {
				return true
			}
		}
	}
	return false
}
