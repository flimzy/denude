package rangeint

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/token"
	"go/version"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "rangeint",
		Doc:  "replaces long-form for lops with range-int loops",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

// rangeBound returns the upper bound of an integer range expression, or
// returns false if it can't be determined. ident is the identifier of the
// loop variable to match against.
func rangeBound(ident, expr ast.Expr) (ast.Expr, bool) {
	cond, ok := expr.(*ast.BinaryExpr)
	if !ok {
		return nil, false
	}
	switch cond.Op {
	case token.LSS, token.LEQ:
		if internal.NamesMatch(ident, cond.X) {
			return cond.Y, true
		}
	case token.GTR, token.GEQ:
		if internal.NamesMatch(ident, cond.Y) {
			return cond.X, true
		}
	}
	return nil, false
}

// isReferenced returns true if ident is referenced in body.
func (a *analyzer) isReferenced(want ast.Expr, body ast.Node) bool {
	wantIdent, ok := want.(*ast.Ident)
	if !ok {
		return false
	}
	wantObj, ok := a.pass.TypesInfo.Defs[wantIdent]
	if !ok {
		wantObj, ok = a.pass.TypesInfo.Uses[wantIdent]
		if !ok {
			return false
		}
	}
	var referenced bool
	ast.Inspect(body, func(node ast.Node) bool {
		if referenced {
			return false
		}
		ident, ok := node.(*ast.Ident)
		if !ok {
			return true
		}
		obj, ok := a.pass.TypesInfo.Uses[ident]
		if !ok {
			return true
		}
		// If the object is the same, then the identifier is referencing the
		// loop variable.
		referenced = obj == wantObj
		return !referenced
	})
	return referenced
}

// isConstantZero returns true if expr is a constant zero value.
func isConstantZero(expr ast.Expr) bool {
	lit, ok := expr.(*ast.BasicLit)
	if !ok {
		return false
	}
	return lit.Kind == token.INT && lit.Value == "0"
}

func (a *analyzer) inspect(node ast.Node) bool {
	forStmt, ok := node.(*ast.ForStmt)
	if !ok {
		return true
	}
	assignment, ok := forStmt.Init.(*ast.AssignStmt)
	if !ok {
		return true
	}
	if len(assignment.Lhs) != 1 {
		return true
	}
	if !isConstantZero(assignment.Rhs[0]) {
		return true
	}
	bound, ok := rangeBound(assignment.Lhs[0], forStmt.Cond)
	if !ok {
		return true
	}
	if a.isReferenced(assignment.Lhs[0], bound) {
		// The loop variable is referenced in the bound expression, so we can't
		// replace the loop with a range loop.
		return true
	}
	if !internal.IsEffectiveIncByOne(assignment.Lhs[0], forStmt.Post) {
		return true
	}

	replacement := &ast.RangeStmt{
		For:  forStmt.For,
		X:    bound,
		Body: forStmt.Body,
	}
	if a.isReferenced(assignment.Lhs[0], forStmt.Body) {
		replacement.Key = assignment.Lhs[0]
		replacement.Tok = assignment.Tok
	}

	oldText := bytes.TrimSuffix(internal.DumpNode(forStmt), internal.DumpNode(forStmt.Body))
	newText := bytes.TrimSuffix(internal.DumpNode(replacement), internal.DumpNode(forStmt.Body))

	a.pass.Report(analysis.Diagnostic{
		Pos: forStmt.Pos(),
		End: forStmt.End(),
		Message: fmt.Sprintf("int loop can be simplified with range: %s ⟹ %s",
			string(bytes.SplitN(oldText, []byte("\n"), 2)[0]),
			string(bytes.SplitN(newText, []byte("\n"), 2)[0]),
		),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     forStmt.Pos(),
						End:     forStmt.Body.Pos(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

const minGoVersion = "go1.22.0"

// run checks for loops that can be simplified with Go 1.22's range int syntax.
//
//	for i := 0; i < 10; i++ { ==. for i := range 10 {
//	for i := 0; i < 10; i += 1 { ==. for i := range 10 {
//	for i := 0; i < 10; i = i + 1 { ==. for i := range 10 {
func run(pass *analysis.Pass) (any, error) {
	// If the module is not using Go 1.22 or later, we can't use the new syntax.
	if version.Compare(pass.Pkg.GoVersion(), minGoVersion) < 0 {
		return nil, nil
	}
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		// If the file has a build constraint for an older Go version, we can't
		// use the new syntax.
		if f.GoVersion != "" && version.Compare(f.GoVersion, minGoVersion) < 0 {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
