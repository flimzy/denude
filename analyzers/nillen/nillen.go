package nillen

import (
	"fmt"
	"go/ast"
	"go/constant"
	"go/token"
	"go/types"

	"golang.org/x/tools/go/analysis"
	"honnef.co/go/tools/analysis/code"
	"honnef.co/go/tools/analysis/lint"
	"honnef.co/go/tools/go/types/typeutil"
	"honnef.co/go/tools/knowledge"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "nillen",
		Doc:  "removes unnecessary nil check with len",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

func (a *analyzer) isConstZero(expr ast.Expr) (isConst, isZero bool) {
	_, ok := expr.(*ast.BasicLit)
	if ok {
		return true, code.IsIntegerLiteral(a.pass, expr, constant.MakeInt64(0))
	}
	id, ok := expr.(*ast.Ident)
	if !ok {
		return false, false
	}
	c, ok := a.pass.TypesInfo.ObjectOf(id).(*types.Const)
	if !ok {
		return false, false
	}
	return true, c.Val().Kind() == constant.Int && c.Val().String() == "0"
}

func (a *analyzer) inspect(node ast.Node) bool {
	// check that expr is "x || y" or "x && y"
	expr, ok := node.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	if expr.Op != token.LOR && expr.Op != token.LAND {
		return true
	}
	eqNil := expr.Op == token.LOR

	// check that x is "xx == nil" or "xx != nil"
	x, ok := expr.X.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	if eqNil && x.Op != token.EQL {
		return true
	}
	if !eqNil && x.Op != token.NEQ {
		return true
	}
	xx, ok := x.X.(*ast.Ident)
	if !ok {
		return true
	}
	if !code.IsNil(a.pass, x.Y) {
		return true
	}

	// check that y is "len(xx) == 0" or "len(xx) ... "
	y, ok := expr.Y.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	if eqNil && y.Op != token.EQL { // must be len(xx) *==* 0
		return true
	}
	yx, ok := y.X.(*ast.CallExpr)
	if !ok {
		return true
	}
	if !code.IsCallTo(a.pass, yx, "len") {
		return true
	}
	yxArg, ok := yx.Args[knowledge.Arg("len.v")].(*ast.Ident)
	if !ok {
		return true
	}
	if yxArg.Name != xx.Name {
		return true
	}

	if eqNil && !code.IsIntegerLiteral(a.pass, y.Y, constant.MakeInt64(0)) { // must be len(x) == *0*
		return true
	}

	if !eqNil {
		isConst, isZero := a.isConstZero(y.Y)
		if !isConst {
			return true
		}
		switch y.Op {
		case token.EQL:
			// avoid false positive for "xx != nil && len(xx) == 0"
			if isZero {
				return true
			}
		case token.GEQ:
			// avoid false positive for "xx != nil && len(xx) >= 0"
			if isZero {
				return true
			}
		case token.NEQ:
			// avoid false positive for "xx != nil && len(xx) != <non-zero>"
			if !isZero {
				return true
			}
		case token.GTR:
			// ok
		default:
			return true
		}
	}

	// finally check that xx type is one of array, slice, map or chan
	// this is to prevent false positive in case if xx is a pointer to an array
	typ := a.pass.TypesInfo.TypeOf(xx)
	ok = typeutil.All(typ, func(term *types.Term) bool {
		switch term.Type().Underlying().(type) {
		case *types.Slice:
			return true
		case *types.Map:
			return true
		case *types.Chan:
			return true
		case *types.Pointer:
			return false
		case *types.TypeParam:
			return false
		default:
			lint.ExhaustiveTypeSwitch(term.Type().Underlying())
			return false
		}
	})
	if !ok {
		return true
	}

	oldText := internal.DumpNode(expr)
	newText := internal.DumpNode(expr.Y)

	a.pass.Report(analysis.Diagnostic{
		Pos:     expr.Pos(),
		End:     expr.End(),
		Message: fmt.Sprintf("nil check redundant with len(): %s ⟹ %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     expr.Pos(),
						End:     expr.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following redundant nil-checks:
//
//	if x == nil || len(x) == 0 {}
//	if x != nil && len(x) != 0 {}
//	if x != nil && len(x) == N {} (where N != 0)
//	if x != nil && len(x) > N {}
//	if x != nil && len(x) >= N {} (where N != 0)
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
