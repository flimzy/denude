package sprint

import (
	"fmt"
	"go/ast"
	"go/token"
	"go/types"
	"path/filepath"
	"slices"
	"strconv"
	"strings"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

const dotImport = "."

// concernedImports is a list of imports that we are concerned with. If an
// import is not in this list, we will not track its usage.
//
//nolint:gochecknoglobals // Effective package const
var concernedImports = []string{
	"fmt",
	"errors",
	"strconv",
}

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "sprint",
		Doc:  "replaces some fmt.Sprint, fmt.Sprintln, and fmt.Sprintf calls with simpler alternatives",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
	// tracks the number of times an import alias is referenced, so that we know
	// if we can remove it. The key is the alias.
	importUses map[string]int
	// These track the existing imports
	foundAliasToPath map[string]string
	foundPathToAlias map[string][]string
	// tracks imports that we have added. The map is in path:alias format.
	addedImports map[string]string
}

func (a *analyzer) isInterestingCall(expr *ast.CallExpr) (string, bool) {
	_, path, ok := internal.ImportInfo(a.pass, expr)
	if !ok {
		return "", false
	}
	var ident *ast.Ident
	switch t := expr.Fun.(type) {
	case *ast.SelectorExpr:
		ident = t.Sel
	case *ast.Ident:
		ident = t
	default:
		return "", false
	}
	if path != "fmt" {
		return "", false
	}
	switch ident.Name {
	case "Sprint", "Sprintln", "Sprintf", "Errorf":
		return ident.Name, true
	}
	return "", false
}

// stringValue returns the string value of expr, if possible to determine, and a
// boolean indicating whether the value was successfully determined. The value
// can be deermined if expr represents a constant or string literal.
func (a *analyzer) stringValue(expr ast.Expr) (string, bool) {
	switch t := expr.(type) {
	case *ast.BasicLit:
		if t.Kind != token.STRING {
			return "", false
		}
		value, err := strconv.Unquote(t.Value)

		return value, err == nil
	case *ast.Ident:
		// If the ident represent a constant string, we can check
		// its value.
		obj := a.pass.TypesInfo.ObjectOf(t)
		if obj == nil {
			return "", false
		}
		value, ok := obj.(*types.Const)
		if !ok {
			return "", false
		}
		return value.Val().String(), true
	}
	return "", false
}

// hasNoFormatVerbs returns true if expr can positively be determined to contain
// no format verbs.
func (a *analyzer) hasNoFormatVerbs(expr ast.Expr) bool {
	value, ok := a.stringValue(expr)
	if !ok {
		return false
	}
	return !strings.Contains(value, "%")
}

func (a *analyzer) incrementImportUses(node ast.Node) {
	alias, path, ok := internal.ImportInfo(a.pass, node)
	if !ok {
		return
	}
	a.foundPathToAlias[path] = append(a.foundPathToAlias[path], alias)
	if _, ok := a.foundAliasToPath[alias]; !ok {
		a.foundAliasToPath[alias] = path
	}
	if !slices.Contains(concernedImports, path) {
		return
	}
	if alias == dotImport {
		alias = dotImport + path
	}
	a.importUses[alias]++
}

func (a *analyzer) decrementImportUses(expr *ast.CallExpr) {
	alias, path, ok := internal.ImportInfo(a.pass, expr)
	if !ok {
		return
	}
	if !slices.Contains(concernedImports, path) {
		return
	}
	if alias == dotImport {
		alias = dotImport + path
	}
	a.importUses[alias]--
}

// addImport adds imp to the list of imports to be inserted, if it is
// not already referenced, and returns the alias to be used to reference it.
func (a *analyzer) addImport(path string) string {
	alias := filepath.Base(path)
	if a.foundAliasToPath[alias] == path {
		// The imort we want is already there, nothing to add
		return alias
	}
	for _, alias := range a.foundPathToAlias[path] {
		if alias != dotImport {
			// The import is already there, so we can just use the alias we
			// found before. But we don't like dot imports, because they are
			// evil.
			return alias
		}
	}

	if a, ok := a.addedImports[path]; ok {
		if a == "" {
			return alias
		}
		// The import is already added, so we can just use the alias we
		// generated before.
		return a
	}

	// The import is not there, so we need to add it.

	// Find a unique alias for the import
	idx := 0
	uniqueAlias := alias
	for {
		if _, ok := a.foundAliasToPath[uniqueAlias]; !ok {
			break
		}
		idx++
		uniqueAlias = alias + strconv.Itoa(idx)
	}

	// Add the import to the list of imports to be inserted
	if uniqueAlias == alias {
		// If we have to use the default alias, then we can just use the
		// package name.
		a.addedImports[path] = ""
	} else {
		a.addedImports[path] = uniqueAlias
	}

	return uniqueAlias
}

// convertExpr applies a type conversion to type typ to value, removing any
// redundant conversions.
//
// Examples:
//
//	int, 3  ==> int(3)
//	string, "foo" ==> string("foo")
//	int, int(3) ==> int(3)
func convertExpr(typ string, value ast.Expr) ast.Expr {
	if !shouldConvert(typ, value) {
		return value
	}

	return &ast.CallExpr{
		Fun:  &ast.Ident{Name: typ},
		Args: []ast.Expr{value},
	}
}

func shouldConvert(typ string, value ast.Expr) bool {
	callExpr, ok := value.(*ast.CallExpr)
	if !ok || len(callExpr.Args) != 1 {
		return true
	}

	ident, ok := callExpr.Fun.(*ast.Ident)
	if !ok {
		return true
	}

	haveType := internal.NameToBasicKind(ident.Name)
	if haveType == types.Invalid {
		return true
	}

	return haveType != internal.NameToBasicKind(typ)
}

// strconv returns an expression that converts value to a string, using the
// appropriate strconv function for the type of value.
func (a *analyzer) strconv(typ types.BasicKind, value ast.Expr) ast.Expr {
	alias := a.addImport("strconv")
	switch typ {
	case types.Int8, types.Int16, types.Int32, types.Uint8, types.Uint16:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "Itoa"},
			},
			Args: []ast.Expr{convertExpr("int", value)},
		}
	case types.Uint32, types.Uint:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "FormatUint"},
			},
			Args: []ast.Expr{
				convertExpr("uint64", value),
				&ast.BasicLit{
					Kind:  token.INT,
					Value: "10",
				},
			},
		}
	case types.Int:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "Itoa"},
			},
			Args: []ast.Expr{value},
		}
	case types.Int64:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "FormatInt"},
			},
			Args: []ast.Expr{value, &ast.BasicLit{
				Kind:  token.INT,
				Value: "10",
			}},
		}
	case types.Uint64:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "FormatUint"},
			},
			Args: []ast.Expr{value, &ast.BasicLit{
				Kind:  token.INT,
				Value: "10",
			}},
		}
	case types.Bool:
		return &ast.CallExpr{
			Fun: &ast.SelectorExpr{
				X:   &ast.Ident{Name: alias},
				Sel: &ast.Ident{Name: "FormatBool"},
			},
			Args: []ast.Expr{value},
		}
	default:
		panic(fmt.Sprintf("unexpected type: %v", typ))
	}
}

// isStringer returns true if the type of s implements the fmt.Stringer.
func (a *analyzer) isStringer(s ast.Expr) bool {
	ident, ok := s.(*ast.Ident)
	if !ok {
		return false
	}
	obj := a.pass.TypesInfo.ObjectOf(ident)
	if obj == nil {
		return false
	}
	typ, ok := obj.Type().(*types.Named)
	if !ok {
		return false
	}
	for i := range typ.NumMethods() {
		if typ.Method(i).Name() == "String" {
			// Success, if String() returns a string
			return typ.Method(i).Type().(*types.Signature).Results().At(0).Type().String() == "string"
		}
	}

	return false
}

// argType takes an expression and returns the type of the argument, possibly
// modifying the argument if necessary to conform to its underlying type.
func (a *analyzer) argType(arg ast.Expr) (ast.Expr, types.BasicKind) {
	kind, ok := internal.ExpressionType(a.pass, arg)
	if ok {
		return arg, kind
	}
	ident, ok := arg.(*ast.Ident)
	if !ok {
		return nil, 0
	}
	obj := a.pass.TypesInfo.ObjectOf(ident)
	if obj == nil {
		return nil, 0
	}
	namedType, ok := obj.Type().(*types.Named)
	if !ok {
		return nil, 0
	}
	underlying, ok := namedType.Underlying().(*types.Basic)
	if !ok {
		return nil, 0
	}
	kind = underlying.Kind()
	arg = &ast.CallExpr{
		Fun: &ast.Ident{
			Name: namedType.Underlying().String(),
		},
		Args: []ast.Expr{arg},
	}
	return arg, kind
}

func (a *analyzer) inspect(node ast.Node) bool {
	switch node.(type) {
	case *ast.SelectorExpr, *ast.Ident:
		a.incrementImportUses(node)
	}

	// Check that we have a expr expression
	expr, ok := node.(*ast.CallExpr)
	if !ok {
		return true
	}

	funcName, ok := a.isInterestingCall(expr)
	if !ok {
		return true
	}

	var value ast.Expr
	switch len(expr.Args) {
	case 0:
		value = &ast.BasicLit{
			Kind:  token.STRING,
			Value: `""`,
		}
	case 1:
		switch funcName {
		case "Sprintf", "Errorf":
			if !a.hasNoFormatVerbs(expr.Args[0]) {
				return true
			}
		}
		if a.isStringer(expr.Args[0]) {
			value = &ast.CallExpr{
				Fun: &ast.SelectorExpr{
					X:   expr.Args[0],
					Sel: &ast.Ident{Name: "String"},
				},
			}
			break
		}

		switch arg, kind := a.argType(expr.Args[0]); kind {
		case types.String:
			value = arg
		case types.Int8, types.Int16, types.Int32, types.Int, types.Int64, types.Uint8, types.Uint16, types.Uint32, types.Uint, types.Uint64, types.Bool:
			value = a.strconv(kind, arg)
		default:
			return true
		}

		if funcName == "Errorf" {
			// Replace fmt.Errorf with errors.New
			alias := a.addImport("errors")
			value = &ast.CallExpr{
				Fun: &ast.SelectorExpr{
					X:   &ast.Ident{Name: alias},
					Sel: &ast.Ident{Name: "New"},
				},
				Args: []ast.Expr{value},
			}
		}
	case 2:
		if funcName != "Sprintf" {
			return true
		}
		format, ok := a.stringValue(expr.Args[0])
		if !ok {
			return true
		}

		if a.isStringer(expr.Args[1]) && (format == "%v" || format == "%s") {
			value = &ast.CallExpr{
				Fun: &ast.SelectorExpr{
					X:   expr.Args[1],
					Sel: &ast.Ident{Name: "String"},
				},
			}
			break
		}

		arg, kind := a.argType(expr.Args[1])
		switch {
		case (format == "%s" || format == "%v") && kind == types.String:
			value = arg
		case (format == "%d" || format == "%v") && slices.Contains([]types.BasicKind{types.Int, types.Int8, types.Int16, types.Int32, types.Int64, types.Uint8, types.Uint16, types.Uint32, types.Uint, types.Uint64}, kind):
			value = a.strconv(kind, arg)
		case (format == "%t" || format == "%v") && kind == types.Bool:
			value = a.strconv(kind, arg)
		case strings.HasPrefix(format, "%s") && kind == types.String:
			value = &ast.BinaryExpr{
				X:  arg,
				Op: token.ADD,
				Y: &ast.BasicLit{
					Kind:  token.STRING,
					Value: strconv.Quote(format[2:]),
				},
			}
		case strings.HasSuffix(format, "%s") && kind == types.String:
			value = &ast.BinaryExpr{
				X: &ast.BasicLit{
					Kind:  token.STRING,
					Value: strconv.Quote(format[:len(format)-2]),
				},
				Op: token.ADD,
				Y:  arg,
			}
		default:
			return true
		}
	case 3:
		if funcName != "Sprintf" {
			return true
		}
		if format, _ := a.stringValue(expr.Args[0]); format != "%s%s" {
			return true
		}
		if !internal.IsString(a.pass, expr.Args[1]) {
			return true
		}
		if !internal.IsString(a.pass, expr.Args[2]) {
			return true
		}
		value = &ast.BinaryExpr{
			X:  expr.Args[1],
			Op: token.ADD,
			Y:  expr.Args[2],
		}
	default:
		return true
	}

	if funcName == "Sprintln" {
		if lit, ok := value.(*ast.BasicLit); ok && lit.Kind == token.STRING && lit.Value[0] == '"' {
			v, err := strconv.Unquote(lit.Value)
			if err != nil {
				// should never happen
				return true
			}
			lit.Value = strconv.Quote(v + "\n")
		} else {
			value = &ast.BinaryExpr{
				X:  value,
				Op: token.ADD,
				Y: &ast.BasicLit{
					Kind:  token.STRING,
					Value: `"\n"`,
				},
			}
		}
	}
	a.decrementImportUses(expr)

	oldText := internal.DumpNode(expr)
	newText := internal.DumpNode(value)

	a.pass.Report(analysis.Diagnostic{
		Pos:     expr.Pos(),
		End:     expr.End(),
		Message: fmt.Sprintf("fmt.%s call can be simplified: %s ⟹ %s", funcName, string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     expr.Pos(),
						End:     expr.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return true
}

func importAlias(node ast.Node) (alias, path string, ok bool) {
	imp, ok := node.(*ast.ImportSpec)
	if !ok {
		return "", "", false
	}
	path, _ = strconv.Unquote(imp.Path.Value)
	if imp.Name == nil {
		return filepath.Base(path), path, true
	}
	switch imp.Name.Name {
	case dotImport:
		return imp.Name.Name + filepath.Base(path), path, true
	case "_":
		return "", "", false
	default:
		return imp.Name.Name, path, true
	}
}

func (a *analyzer) inspectImports(node ast.Node) bool {
	if alias, path, ok := importAlias(node); ok && slices.Contains(concernedImports, path) {
		a.importUses[alias] = 0
	}

	// Return true for nodes that can contain *ast.ImportSpec nodes, false otherwise
	// In this case, we only care about the file (which contains imports) and GenDecl (general declarations) nodes
	switch node.(type) {
	case *ast.File, *ast.GenDecl:
		return true
	default:
		return false
	}
}

func (a *analyzer) shouldPruneImports() bool {
	for _, uses := range a.importUses {
		if uses == 0 {
			return true
		}
	}
	return false
}

func (a *analyzer) pruneImports(node ast.Node) bool {
	if _, ok := node.(*ast.File); ok {
		return true
	}
	decl, ok := node.(*ast.GenDecl)
	if !ok {
		return false
	}

	if decl.Tok != token.IMPORT {
		return false
	}
	switch len(decl.Specs) {
	case 0:
		return false
	case 1:
		alias, path, ok := importAlias(decl.Specs[0])
		if !ok || !slices.Contains(concernedImports, path) {
			return false
		}
		if a.importUses[alias] == 0 {
			oldText := internal.DumpNode(decl)

			a.pass.Report(analysis.Diagnostic{
				Pos:     decl.Pos(),
				End:     decl.End(),
				Message: fmt.Sprintf("Remove unused import: %s", string(oldText)),
				SuggestedFixes: []analysis.SuggestedFix{
					{
						TextEdits: []analysis.TextEdit{
							{
								Pos:     decl.Pos(),
								End:     decl.End(),
								NewText: nil,
							},
						},
					},
				},
			})
		}
	}
	toDelete := make([]ast.Spec, 0, len(decl.Specs))
	toDeleteAliases := make([]string, 0, len(decl.Specs))
	for _, spec := range decl.Specs {
		alias, path, ok := importAlias(spec)
		if !ok || !slices.Contains(concernedImports, path) {
			continue
		}
		if a.importUses[alias] == 0 {
			toDelete = append(toDelete, spec)
			if strings.HasPrefix(alias, dotImport) {
				alias, _ = strconv.Unquote(spec.(*ast.ImportSpec).Path.Value)
			}
			toDeleteAliases = append(toDeleteAliases, alias)
		}
	}
	switch len(toDelete) {
	case 0:
		return false
	case len(decl.Specs):
		// delete the entire import declaration
		a.pass.Report(analysis.Diagnostic{
			Pos:     decl.Pos(),
			End:     decl.End(),
			Message: fmt.Sprintf("Remove unused imports: %s", strings.Join(toDeleteAliases, ", ")),
			SuggestedFixes: []analysis.SuggestedFix{
				{
					TextEdits: []analysis.TextEdit{
						{
							Pos:     decl.Pos(),
							End:     decl.End(),
							NewText: nil,
						},
					},
				},
			},
		})
		return false
	}
	for _, spec := range toDelete {
		oldText := internal.DumpNode(spec)
		a.pass.Report(analysis.Diagnostic{
			Pos:     spec.Pos(),
			End:     spec.End(),
			Message: fmt.Sprintf("Remove unused import: %s", string(oldText)),
			SuggestedFixes: []analysis.SuggestedFix{
				{
					TextEdits: []analysis.TextEdit{
						{
							Pos:     spec.Pos(),
							End:     spec.End(),
							NewText: nil,
						},
					},
				},
			},
		})
	}
	return false
}

func (a *analyzer) addImports(node ast.Node) bool {
	file, ok := node.(*ast.File)
	if !ok {
		return false
	}

	decl := &ast.GenDecl{
		Tok: token.IMPORT,
	}

	newImports := make([]string, len(a.addedImports))
	for path, alias := range a.addedImports {
		newImports = append(newImports, alias)
		spec := &ast.ImportSpec{
			Path: &ast.BasicLit{
				Kind:  token.STRING,
				Value: strconv.Quote(path),
			},
		}
		if alias != "" {
			spec.Name = &ast.Ident{
				Name: alias,
			}
		}
		decl.Specs = append(decl.Specs, spec)
	}

	newText := append([]byte("\n\n"), internal.DumpNode(decl)...)

	a.pass.Report(analysis.Diagnostic{
		Pos:     file.Name.End(),
		Message: fmt.Sprintf("Add new imports: %s", strings.Join(newImports, ",")),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     file.Name.End(),
						End:     file.Name.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following Sprint calls that can be simplified:
//
//	fmt.Sprint("string literal") => "string literal"
//	fmt.Sprint(stringVar) => stringVar
//	fmt.Sprint(stringExpression()) => stringExpression()
//	fmt.Sprint() => ""
//	fmt.Sprintln() => "\n"
//	fmt.Sprintf("foo") => "foo"
//	fmt.Sprintf("%s", x) => x
//	fmt.Sprintf("%s foo", x) => x + " foo"
//	fmt.Sprintf("foo %s", x) => "foo " + x
//	fmt.Sprintf("%s%s", x, y) => x + y
//	fmt.Errorf("no args") => errors.New("no args")
//	fmt.Sprint(3) => strconv.Itoa(3)
//	fmt.Sprint(true) => strconv.FormatBool(true)
//	fmt.Sprintf("%v", string) => string
//	fmt.Sprintf("%v", int) => int
//	fmt.Sprintf("%v", bool) => bool
//	fmt.Sprintf("%s", stringer) => stringer.String()
//	fmt.Sprintf("%v", stringer) => stringer.String() (??)
//	fmt.Sprintf("%s", myString) => string(myString)
//
// TODO:
//
//	fmt.Sprintf("%v", float) => float
//	fmt.Sprint(3.14) => strconv.FormatFloat(3.14, 'f', -1, 64)
//	fmt.Sprint(3.14) => strconv.FormatFloat(3.14, 'f', -1, 32)
func run(pass *analysis.Pass) (any, error) {
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		a := &analyzer{
			pass:             pass,
			importUses:       make(map[string]int),
			foundAliasToPath: map[string]string{},
			foundPathToAlias: map[string][]string{},
			addedImports:     map[string]string{},
		}
		ast.Inspect(f, a.inspectImports)
		ast.Inspect(f, a.inspect)
		if a.shouldPruneImports() {
			ast.Inspect(f, a.pruneImports)
		}
		if len(a.addedImports) > 0 {
			ast.Inspect(f, a.addImports)
		}
	}
	return nil, nil
}
