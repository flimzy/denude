package opcombine

import (
	"fmt"
	"go/ast"
	"go/token"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "opcombine",
		Doc:  "combines arithmetic operators",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

// getOtherOperand checks that the first operand of expr matches lhs, or for
// operations where order is insignificant, it will also check the second
// operand matches lhs. If a match is found, the operator and other operaned are
// returned. If no match is found, the final return value will be false.
func (*analyzer) getOtherOperand(lhs, expr ast.Expr) (token.Token, ast.Expr, bool) {
	binExpr, ok := expr.(*ast.BinaryExpr)
	if !ok {
		return token.ILLEGAL, nil, false
	}
	if internal.NamesMatch(lhs, binExpr.X) {
		return binExpr.Op, binExpr.Y, true
	}
	switch binExpr.Op {
	case token.ADD, token.MUL, token.AND, token.OR, token.XOR:
	default:
		return token.ILLEGAL, nil, false
	}

	if internal.NamesMatch(lhs, binExpr.Y) {
		return binExpr.Op, binExpr.X, true
	}
	return token.ILLEGAL, nil, false
}

func (a *analyzer) inspect(node ast.Node) bool {
	stmt, ok := node.(*ast.AssignStmt)
	if !ok {
		return true
	}

	if len(stmt.Lhs) != 1 {
		return true
	}
	if internal.IsString(a.pass, stmt.Lhs[0]) {
		// Don't simplify string concatenation, as it can be less efficient.
		// See https://medium.com/@samolazov.herman/go-myths-we-believe-strings-cad8d53e0c20
		return true
	}

	var newStmt ast.Node

	switch stmt.Tok {
	case token.ASSIGN:
		op, other, ok := a.getOtherOperand(stmt.Lhs[0], stmt.Rhs[0])
		if !ok {
			return true
		}

		y, _ := other.(*ast.BasicLit)
		switch {
		case y != nil && y.Kind == token.INT && y.Value == "1":
			newOp := token.ILLEGAL
			switch op {
			case token.ADD:
				newOp = token.INC
			case token.SUB:
				newOp = token.DEC
			}
			newStmt = &ast.IncDecStmt{
				X:   stmt.Lhs[0],
				Tok: newOp,
			}
		default:
			newOp := token.ILLEGAL
			switch op {
			case token.ADD:
				newOp = token.ADD_ASSIGN
			case token.SUB:
				newOp = token.SUB_ASSIGN
			case token.MUL:
				newOp = token.MUL_ASSIGN
			case token.QUO:
				newOp = token.QUO_ASSIGN
			case token.REM:
				newOp = token.REM_ASSIGN
			case token.SHL:
				newOp = token.SHL_ASSIGN
			case token.SHR:
				newOp = token.SHR_ASSIGN
			case token.AND:
				newOp = token.AND_ASSIGN
			case token.OR:
				newOp = token.OR_ASSIGN
			case token.XOR:
				newOp = token.XOR_ASSIGN
			case token.AND_NOT:
				newOp = token.AND_NOT_ASSIGN
			}

			newStmt = &ast.AssignStmt{
				Lhs: []ast.Expr{stmt.Lhs[0]},
				Tok: newOp,
				Rhs: []ast.Expr{other},
			}
		}
	case token.ADD_ASSIGN:
		y, ok := stmt.Rhs[0].(*ast.BasicLit)
		if !ok {
			return true
		}
		if y.Kind != token.INT {
			return true
		}
		if y.Value != "1" {
			return true
		}
		newStmt = &ast.IncDecStmt{
			X:   stmt.Lhs[0],
			Tok: token.INC,
		}
	case token.SUB_ASSIGN:
		y, ok := stmt.Rhs[0].(*ast.BasicLit)
		if !ok {
			return true
		}
		if y.Kind != token.INT {
			return true
		}
		if y.Value != "1" {
			return true
		}
		newStmt = &ast.IncDecStmt{
			X:   stmt.Lhs[0],
			Tok: token.DEC,
		}
	default:
		return true
	}

	oldText := internal.DumpNode(stmt)
	newText := internal.DumpNode(newStmt)

	a.pass.Report(analysis.Diagnostic{
		Pos:     stmt.Pos(),
		End:     stmt.End(),
		Message: fmt.Sprintf("arithmetic expression can be simplified: %s ⟹ %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     stmt.Pos(),
						End:     stmt.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following arithmetic expressions that can be simplified:
//
//	x = x + 1 ==> x++
//	x = 1 + x ==> x++
//	x = x - 1 ==> x--
//	x += 1 ==> x++
//	x -= 1 ==> x--
//	x = x + 3 ==> x += 3
//	x = 3 + x ==> x += 3
//	x = x - 3 ==> x -= 3
//	x = x * 3 ==> x *= 3
//	x = 3 * x ==> x *= 3
//	x = x / 3 ==> x /= 3
//	x = x % 3 ==> x %= 3
//	x = x << 3 ==> x <<= 3
//	x = x >> 3 ==> x >>= 3
//	x = x & 3 ==> x &= 3
//	x = 3 & x ==> x &= 3
//	x = x | 3 ==> x |= 3
//	x = 3 | x ==> x |= 3
//	x = x ^ 3 ==> x ^= 3
//	x = 3 ^ x ==> x ^= 3
//	x = x &^ 3 ==> x &^= 3
//	x = x + y ==> x += y
//	x = y + x ==> x += y
//	x = x + foo() ==> x += foo()
//	x = foo() + x ==> x += foo()
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
