package unparend

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/token"
	"slices"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "unparend",
		Doc:  "removes unnecessary parenthesis",
		Run:  run,
	}
}

type update struct {
	Pos token.Pos
	End token.Pos
}

type analyzer struct {
	stack   []ast.Node
	pass    *analysis.Pass
	changed map[ast.Node]bool
	updates []update
}

func (a *analyzer) inspect(node ast.Node) bool {
	if node == nil {
		if len(a.stack) > 0 {
			a.stack = a.stack[:len(a.stack)-1]
		}
		return true
	}
	a.stack = append(a.stack, node)
	expr, ok := node.(ast.Expr)
	if !ok {
		return true
	}

	replacement, simplified := a.simplifyExpr(expr)

	if simplified {
		a.replace(node, replacement)
		return false
	}
	return true
}

func (a *analyzer) simplifyExpr(node ast.Expr) (ast.Expr, bool) {
	switch n := node.(type) {
	case *ast.ParenExpr:
		return a.simplifyParenExpr(n)
	case *ast.BinaryExpr:
		return a.simplifyBinaryExpr(n)
	}
	return node, false
}

func run(pass *analysis.Pass) (any, error) {
	const defaultStackSize = 20
	a := analyzer{
		stack:   make([]ast.Node, 0, defaultStackSize),
		pass:    pass,
		changed: make(map[ast.Node]bool),
	}

	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}

// simplifyParenExpr simplifies a parenthesized expression. It returns the
// simplified expression, or the original expression if simplification is not
// possible. The boolean will be true if simplification was possible.
func (a analyzer) simplifyParenExpr(n *ast.ParenExpr) (ast.Expr, bool) {
	if _, ok := a.changed[n]; ok {
		return n, false
	}
	a.changed[n] = true
	switch child := n.X.(type) {
	case *ast.ParenExpr, *ast.Ident, *ast.BasicLit, *ast.CallExpr, *ast.SelectorExpr:
		r, _ := a.simplifyExpr(child)
		return r, true
	case *ast.BinaryExpr:
		parent := a.stack[len(a.stack)-2]
		if _, ok := parent.(*ast.AssignStmt); ok {
			r, _ := a.simplifyExpr(child)
			return r, true
		}
	}
	return n, false
}

// precedence returns the precedence of op, but for logical OR, it returns the
// precedence of logical AND, because the precedence of these operators confuses
// many people, so for readability we won't strip these parenthesis, even if
// they aren't strictly needed.
func precedence(op token.Token) int {
	if op == token.LOR {
		return token.LAND.Precedence()
	}
	return op.Precedence()
}

// parenBinayExpr returns the binary expression if the node is a parenthesized
// binary expression, and a boolean indicating whether the conversion was
// successful.
func parenBinayExpr(n ast.Node) (*ast.BinaryExpr, bool) {
	child, ok := n.(*ast.ParenExpr)
	if !ok {
		return nil, false
	}
	binary, ok := child.X.(*ast.BinaryExpr)
	return binary, ok
}

func (a *analyzer) simplifyBinaryExpr(n *ast.BinaryExpr) (ast.Expr, bool) {
	if _, ok := a.changed[n]; ok {
		return n, false
	}
	a.changed[n] = true
	xParen, xOk := parenBinayExpr(n.X)
	yParen, yOk := parenBinayExpr(n.Y)
	if !xOk && !yOk {
		return n, false
	}
	var updated bool
	x, y := n.X, n.Y
	if xOk && shouldRemoveParendsBetween(n.Op, xParen.Op) {
		if _, ok := a.changed[n.X]; !ok {
			a.changed[n.X] = true
			x, _ = a.simplifyBinaryExpr(xParen)
			updated = true
		}
	}
	if yOk && shouldRemoveParendsBetween(n.Op, yParen.Op) {
		if _, ok := a.changed[n.Y]; !ok {
			a.changed[n.Y] = true
			y, _ = a.simplifyBinaryExpr(yParen)
			updated = true
		}
	}
	if updated {
		newX, _ := a.simplifyExpr(x)
		newY, _ := a.simplifyExpr(y)
		r := &ast.BinaryExpr{
			X:  newX,
			Op: n.Op,
			Y:  newY,
		}
		return r, true
	}
	return n, false
}

// shouldRemoveParendsBetween returns true if the parenthesis between the
// parentOp and childOp should be removed. It will return true only if removal
// does not change the meaning, and if the removal would not hurt readability.
func shouldRemoveParendsBetween(parentOp, childOp token.Token) bool {
	if parentOp == childOp {
		switch parentOp {
		// Operators where the precedence and readability are both unaltered by parenthesis.
		case token.LAND, token.LOR, token.ADD, token.SUB, token.MUL:
			return true
		}
	}
	return precedence(parentOp) < precedence(childOp)
}

func (a *analyzer) replace(oldNode, newNode ast.Node) {
	for _, u := range a.updates {
		if oldNode.Pos() >= u.Pos && oldNode.End() <= u.End {
			return
		}
	}
	oldText := internal.DumpNode(oldNode)
	newText := internal.DumpNode(newNode)
	if bytes.Equal(oldText, newText) {
		// Sometimes we remove parenthesis that go/printer puts back in. This
		// is a last check to make sure we don't suggest a fix that doesn't
		// change anything.
		return
	}
	a.updates = append(a.updates, update{
		Pos: oldNode.Pos(),
		End: oldNode.End(),
	})
	slices.SortFunc(a.updates, func(a, b update) int {
		if a.Pos == b.Pos {
			return int(a.End - b.End)
		}
		return int(a.Pos - b.Pos)
	})
	a.pass.Report(analysis.Diagnostic{
		Pos:     oldNode.Pos(),
		End:     oldNode.End(),
		Message: fmt.Sprintf("remove unnecessary parenthesis: %s ⟹ %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     oldNode.Pos(),
						End:     oldNode.End(),
						NewText: newText,
					},
				},
			},
		},
	})
}
