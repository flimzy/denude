package neglen

import (
	"fmt"
	"go/ast"
	"go/constant"
	"go/token"
	"go/types"

	"golang.org/x/tools/go/analysis"
	"honnef.co/go/tools/analysis/code"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "neglen",
		Doc:  "replaces len(x) <= 0 with len(x) == 0",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

func (a *analyzer) isConstZero(expr ast.Expr) bool {
	_, ok := expr.(*ast.BasicLit)
	if ok {
		return true && code.IsIntegerLiteral(a.pass, expr, constant.MakeInt64(0))
	}
	id, ok := expr.(*ast.Ident)
	if !ok {
		return false
	}
	c, ok := a.pass.TypesInfo.ObjectOf(id).(*types.Const)
	if !ok {
		return false
	}
	return true && c.Val().Kind() == constant.Int && c.Val().String() == "0"
}

// isLenExpr returns true if expr is a call to len
func isLenExpr(expr ast.Expr) bool {
	call, ok := expr.(*ast.CallExpr)
	if !ok {
		return false
	}
	id, ok := call.Fun.(*ast.Ident)
	if !ok {
		return false
	}

	return id.Name == "len"
}

func (a *analyzer) inspect(node ast.Node) bool {
	// Check that we have a binary expression
	expr, ok := node.(*ast.BinaryExpr)
	if !ok {
		return true
	}
	// Check that the operator is <= or >=
	if expr.Op != token.LEQ && expr.Op != token.GEQ {
		return true
	}

	switch {
	case isLenExpr(expr.X) && a.isConstZero(expr.Y),
		isLenExpr(expr.Y) && a.isConstZero(expr.X):
	default:
		return true
	}

	oldText := internal.DumpNode(expr)
	newText := internal.DumpNode(&ast.BinaryExpr{
		X:  expr.X,
		Op: token.EQL,
		Y:  expr.Y,
	})

	a.pass.Report(analysis.Diagnostic{
		Pos:     expr.Pos(),
		End:     expr.End(),
		Message: fmt.Sprintf("negative len is impossible: %s ⟹ %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     expr.Pos(),
						End:     expr.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following len checks:
//
//	if len(x) <= 0 {}
//	if 0 >= len(x) {}
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
