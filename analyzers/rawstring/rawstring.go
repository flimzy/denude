package rawstring

import (
	"fmt"
	"go/ast"
	"go/token"
	"strconv"
	"strings"

	"golang.org/x/tools/go/analysis"

	"gitlab.com/flimzy/denude/internal"
)

// Analyzer is the unparend analyzer.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name: "rawstring",
		Doc:  "Replaces string literals with raw strings where it can reduce the number of escape characters",
		Run:  run,
	}
}

type analyzer struct {
	pass *analysis.Pass
}

func (a *analyzer) inspect(node ast.Node) bool {
	expr, ok := node.(*ast.BasicLit)
	if !ok {
		return true
	}
	if expr.Kind != token.STRING {
		return true
	}
	if expr.Value[0] != '"' {
		return true
	}
	// Honor \u, \U, and \x escape sequences
	if strings.Contains(expr.Value, `\u`) ||
		strings.Contains(expr.Value, `\U`) ||
		strings.Contains(expr.Value, `\x`) {
		return true
	}
	value, err := strconv.Unquote(expr.Value)
	if err != nil {
		// should never happen; this would mean a syntax error, which should
		// have been caught by the parser before we got here.
		return true
	}
	if len(value)+2 >= len(expr.Value) {
		// No gain by converting to a raw string
		return true
	}
	if strings.ContainsAny(value, "`\r\n\t") {
		// Can't `-quote a string that contains backticks, and certain other
		// characters that can hurt readability are skipped.
		return true
	}

	replacement := &ast.BasicLit{
		Kind:  token.STRING,
		Value: "`" + value + "`",
	}

	oldText := internal.DumpNode(expr)
	newText := internal.DumpNode(replacement)

	a.pass.Report(analysis.Diagnostic{
		Pos:     expr.Pos(),
		End:     expr.End(),
		Message: fmt.Sprintf("string literal can be simplified as raw string: %s => %s", string(oldText), string(newText)),
		SuggestedFixes: []analysis.SuggestedFix{
			{
				TextEdits: []analysis.TextEdit{
					{
						Pos:     expr.Pos(),
						End:     expr.End(),
						NewText: newText,
					},
				},
			},
		},
	})

	return false
}

// run checks for the following Sprint calls that can be simplified:
//
//	fmt.Sprint("string literal") => "string literal"
//	fmt.Sprint(stringVar) => stringVar
//	fmt.Sprint(stringExpression()) => stringExpression()
//
// TODO:
//
//	fmt.Sprint() => ""
//	fmt.Sprintln() => "\n"
func run(pass *analysis.Pass) (any, error) {
	a := &analyzer{pass: pass}
	for _, f := range pass.Files {
		if internal.IsGenerated(f) {
			continue
		}
		ast.Inspect(f, a.inspect)
	}
	return nil, nil
}
