package main

import (
	"golang.org/x/tools/go/analysis"
	"golang.org/x/tools/go/analysis/multichecker"

	"gitlab.com/flimzy/denude/analyzers/lenloopguard"
	"gitlab.com/flimzy/denude/analyzers/neglen"
	"gitlab.com/flimzy/denude/analyzers/nillen"
	"gitlab.com/flimzy/denude/analyzers/opcombine"
	"gitlab.com/flimzy/denude/analyzers/rangeint"
	"gitlab.com/flimzy/denude/analyzers/rawstring"
	"gitlab.com/flimzy/denude/analyzers/simplebool"
	"gitlab.com/flimzy/denude/analyzers/sprint"
	"gitlab.com/flimzy/denude/analyzers/strlen"
	"gitlab.com/flimzy/denude/analyzers/unparend"
)

func main() {
	analyzers := []*analysis.Analyzer{
		simplebool.Analyzer(),
		unparend.Analyzer(),
		nillen.Analyzer(),
		neglen.Analyzer(),
		sprint.Analyzer(),
		rawstring.Analyzer(),
		opcombine.Analyzer(),
		rangeint.Analyzer(),
		lenloopguard.Analyzer(),
		strlen.Analyzer(),
	}

	multichecker.Main(analyzers...)
}
