package internal

import (
	"go/ast"
	"go/token"
	"go/types"
	"regexp"

	"golang.org/x/tools/go/analysis"
)

var rxCodeGenerated = regexp.MustCompile(`^// Code generated .* DO NOT EDIT\.$`)

// IsGenerated returns true if the file is generated.
func IsGenerated(file *ast.File) bool {
	for _, cg := range file.Comments {
		if cg.Pos() > file.Package {
			return false
		}
		for _, line := range cg.List {
			if rxCodeGenerated.MatchString(line.Text) {
				return true
			}
		}
	}
	return false
}

// IsString returns true if the type of s is "string"
func IsString(pass *analysis.Pass, s ast.Expr) bool {
	switch t := s.(type) {
	case *ast.BasicLit:
		return t.Kind == token.STRING
	case *ast.Ident:
		obj := pass.TypesInfo.ObjectOf(t)
		if obj == nil {
			return false
		}
		typ, ok := obj.Type().(*types.Basic)
		if !ok {
			return false
		}
		return typ.Info()&types.IsString != 0
	case *ast.IndexExpr:
		return IsString(pass, t.X)
	case *ast.SelectorExpr:
		return IsString(pass, t.Sel)
	case *ast.CallExpr:
		var results *ast.FieldList
		switch t := t.Fun.(type) {
		case *ast.Ident:
			if t.Name == "string" {
				return true
			}
			if t.Obj == nil {
				return false
			}
			decl, ok := t.Obj.Decl.(*ast.FuncDecl)
			if !ok {
				return false
			}
			results = decl.Type.Results
		case *ast.FuncLit:
			results = t.Type.Results
		}
		if results == nil || len(results.List) != 1 {
			return false
		}
		typ := pass.TypesInfo.TypeOf(results.List[0].Type)
		if typ == nil {
			return false
		}
		basic, ok := typ.(*types.Basic)
		if !ok {
			return false
		}
		return basic.Info()&types.IsString != 0
	}
	return false
}

// ExpressionType returns the type of the expression s, if it is of a basic type.
func ExpressionType(pass *analysis.Pass, s ast.Expr) (types.BasicKind, bool) {
	switch t := s.(type) {
	case *ast.BasicLit:
		switch t.Kind {
		case token.INT:
			return types.Int, true
		case token.FLOAT:
			return types.Float64, true
		case token.IMAG:
			return types.Complex128, true
		case token.CHAR:
			return types.Rune, true
		case token.STRING:
			return types.String, true
		default:
			return 0, false
		}
	case *ast.Ident:
		obj := pass.TypesInfo.ObjectOf(t)
		if obj == nil {
			return 0, false
		}
		typ, ok := obj.Type().(*types.Basic)
		if !ok {
			return 0, false
		}
		switch typ.Kind() {
		case types.UntypedBool:
			return types.Bool, true
		case types.UntypedInt:
			return types.Int, true
		case types.UntypedRune:
			return types.Rune, true
		case types.UntypedFloat:
			return types.Float64, true
		case types.UntypedComplex:
			return types.Complex128, true
		case types.UntypedString:
			return types.String, true
		}
		return typ.Kind(), true
	case *ast.IndexExpr:
		return ExpressionType(pass, t.X)
	case *ast.SelectorExpr:
		return ExpressionType(pass, t.Sel)
	case *ast.CallExpr:
		var results *ast.FieldList
		switch t := t.Fun.(type) {
		case *ast.Ident:
			typ := NameToBasicKind(t.Name)
			if typ != types.Invalid {
				return typ, true
			}
			if t.Obj == nil {
				return 0, false
			}
			decl, ok := t.Obj.Decl.(*ast.FuncDecl)
			if !ok {
				return 0, false
			}
			results = decl.Type.Results
		case *ast.FuncLit:
			results = t.Type.Results
		}
		if results == nil || len(results.List) != 1 {
			return 0, false
		}
		typ := pass.TypesInfo.TypeOf(results.List[0].Type)
		if typ == nil {
			return 0, false
		}
		basic, ok := typ.(*types.Basic)
		if !ok {
			return 0, false
		}
		return basic.Kind(), true
	}
	return 0, false
}

// NameToBasicKind returns the basic kind of the type with the given name.
func NameToBasicKind(name string) types.BasicKind {
	switch name {
	case "string":
		return types.String
	case "int":
		return types.Int
	case "float32":
		return types.Float32
	case "float64":
		return types.Float64
	case "complex64":
		return types.Complex64
	case "complex128":
		return types.Complex128
	case "rune":
		return types.Rune
	case "byte":
		return types.Byte
	case "int8":
		return types.Int8
	case "int16":
		return types.Int16
	case "int32":
		return types.Int32
	case "int64":
		return types.Int64
	case "uint":
		return types.Uint
	case "uint8":
		return types.Uint8
	case "uint16":
		return types.Uint16
	case "uint32":
		return types.Uint32
	case "uint64":
		return types.Uint64
	case "bool":
		return types.Bool
	}
	return types.Invalid
}

// ImportInfo returns the alias and path of the import referenced by node.
//
// If node is an import spec, the alias is the name of the imported package and
// the path is the import path. If node is a selector expression, the alias is
// the package name and the path is the import path. If node is a call
// expression, the alias is the package name and the path is the import path.
//
// Alais is always populated, either with the package name or the package alias.
//
// If node is not an import spec, selector expression, or call expression, ok is
// false.
func ImportInfo(pass *analysis.Pass, node ast.Node) (alias, path string, ok bool) {
	if node == nil {
		return "", "", false
	}

	var ident *ast.Ident
	switch t := node.(type) {
	case *ast.CallExpr:
		return ImportInfo(pass, t.Fun)
	case *ast.SelectorExpr:
		ident = t.Sel
		switch t2 := t.X.(type) {
		case *ast.Ident:
			alias = t2.Name
		case *ast.SelectorExpr:
			alias = t2.Sel.Name
		default:
			return "", "", false
		}
	case *ast.Ident:
		ident = t
	default:
		return "", "", false
	}
	obj := pass.TypesInfo.ObjectOf(ident)
	if obj == nil {
		return "", "", false
	}
	pkg := obj.Pkg()
	if pkg == nil {
		return "", "", false
	}
	return alias, pkg.Path(), true
}

// NamesMatch returns true if x and y are of the same type and have the same name.
func NamesMatch(x, y ast.Expr) bool {
	switch x := x.(type) {
	case *ast.Ident:
		if y, ok := y.(*ast.Ident); ok {
			return x.Name == y.Name
		}
	case *ast.IndexExpr:
		if y, ok := y.(*ast.IndexExpr); ok {
			return NamesMatch(x.X, y.X) && NamesMatch(x.Index, y.Index)
		}
	case *ast.BasicLit:
		if y, ok := y.(*ast.BasicLit); ok {
			return x.Value == y.Value
		}
	case *ast.SelectorExpr:
		if y, ok := y.(*ast.SelectorExpr); ok {
			return NamesMatch(x.X, y.X) && NamesMatch(x.Sel, y.Sel)
		}
	}

	return false
}

// IsLiteralZero returns true if expr is a literal zero.
func IsLiteralZero(expr ast.Expr) bool {
	lit, ok := expr.(*ast.BasicLit)
	if !ok {
		return false
	}
	return lit.Value == "0"
}
