package internal

import (
	"go/ast"

	"golang.org/x/tools/go/analysis"
	"honnef.co/go/tools/analysis/code"
	"honnef.co/go/tools/knowledge"
)

// LenOf returns true and the argument of the len call if expr is a call to len,
func LenOf(pass *analysis.Pass, expr ast.Expr) (ast.Expr, bool) {
	call, ok := expr.(*ast.CallExpr)
	if !ok {
		return nil, false
	}
	if !code.IsCallTo(pass, call, "len") {
		return nil, false
	}
	return call.Args[knowledge.Arg("len.v")], true
}
