package internal

import (
	"bytes"
	"go/ast"
	"go/printer"
	"go/token"
)

// DumpNode returns the source code representation of the given node.
func DumpNode(node ast.Node) []byte {
	if node == nil {
		return nil
	}
	var buf bytes.Buffer
	if err := printer.Fprint(&buf, token.NewFileSet(), node); err != nil {
		panic(err)
	}
	return buf.Bytes()
}
