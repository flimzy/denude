package internal

import (
	"go/ast"
	"go/token"
)

// IsEffectiveIncByOne returns true if the increment statement is an effective
// increment by one. This will return true for if inc increments ident with an
// expression of the form 'i++', 'i += 1' and 'i = i + 1'.
func IsEffectiveIncByOne(ident ast.Expr, inc ast.Stmt) bool {
	switch incStmt := inc.(type) {
	case *ast.IncDecStmt:
		if incStmt.Tok == token.INC && NamesMatch(ident, incStmt.X) {
			return true
		}
	case *ast.AssignStmt:
		if len(incStmt.Lhs) != 1 {
			return false
		}
		if !NamesMatch(ident, incStmt.Lhs[0]) {
			return false
		}
		if len(incStmt.Rhs) != 1 {
			return false
		}
		switch incStmt.Tok {
		case token.ASSIGN:
			binExpr, ok := incStmt.Rhs[0].(*ast.BinaryExpr)
			if !ok {
				return false
			}
			if binExpr.Op != token.ADD {
				return false
			}
			var other ast.Expr
			switch {
			case NamesMatch(ident, binExpr.X):
				other = binExpr.Y
			case NamesMatch(ident, binExpr.Y):
				other = binExpr.X
			default:
				return false
			}

			basicLit, ok := other.(*ast.BasicLit)
			if !ok {
				return false
			}
			if basicLit.Kind != token.INT {
				return false
			}
			return basicLit.Value == "1"
		case token.ADD_ASSIGN:
			if !NamesMatch(ident, incStmt.Lhs[0]) {
				return false
			}
			basic, ok := incStmt.Rhs[0].(*ast.BasicLit)
			if !ok {
				return false
			}
			if basic.Kind != token.INT {
				return false
			}
			return basic.Value == "1"
		}
	}
	return false
}
