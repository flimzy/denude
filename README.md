# denude

**denude** is a [Go](https://go.dev/) analyzer and rewriter that simplifies your Go code.

It is inspired by a number of linters and rules found in [golangci-lint](https://golangci-lint.run/), and the desire to have those rules which are safe to apply automatically, done automatically.

To be included in **denude**, a rule must meet the following criteria:

- The alternative is functionally identical to its original
- The alternative has the same or better performance charactaristics as the original
- The alternative is unambiguously as easy or easier to read than the original

## License

**denude** is licensed under the same terms as Go itself. See the [LICENSE](LICENCE) file.